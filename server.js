'use strict';
/**
 * Created by Dominic on 16-Sep-2014.
 */
var mongoose = require('mongoose'),
    swig = require('swig'),
    path = require('path'),
    settings = require('./server/config/settings');


var express = require('express');

var app = express();

app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', __dirname + '/server/views');
//var db = mongoose.connect('mongodb://localhost/databank');

var db = mongoose.connect(settings.db, function(err) {
    if (err) {
        console.error('\x1b[31m', 'Could not connect to MongoDB!');
        console.log(err);
    }
});



require('./server/config/passport')();
require('./server/config/express')(app, db);

app.use(express.static(path.resolve('./public')));

var port = process.env.NODE_ENV || 3041;




app.listen(port);
console.log('Application Started on port '+ port);