'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
    AccountManager = require('../account');

module.exports = function() {
	// Use local strategy
	passport.use(new LocalStrategy({
			usernameField: 'account_name',
			passwordField: 'password'
		},
		function(account_name, password, done) {
			AccountManager.authenticate(account_name, password, function(err, account) {
				if (err) {
					return done(err);
				}
                if(account){
                    return done(null, account);
                }

			});
		}
	));
};