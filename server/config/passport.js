'use strict';

var passport = require('passport'),
	path = require('path'),
    AccountManager = require('../account');

module.exports = function() {
	// Serialize sessions
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	// Deserialize sessions
	passport.deserializeUser(function(id, done) {
		AccountManager.getAccountById(id, function(err, user) {
			done(err, user);
		});
	});

	require('./passport-local')();
};