'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
	return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
	return (this.provider !== 'local' || (password && password.length > 6));
};

/**
 * Account Schema
 */
var AccountSchema = new Schema({
    email: {
        type: String,
        trim: true,
        default: '',
        validate: [validateLocalStrategyProperty, 'Please fill in your email'],
        match: [/.+\@.+\..+/, 'Please fill a valid email address']
    },
	account_name: {
		type: String,
		unique: 'testing error message',
		required: 'Please fill in a account_name',
		trim: true
	},
	password: {
		type: String,
		default: '',
		validate: [validateLocalStrategyPassword, 'Password should be longer']
	},
	salt: {
		type: String
	},
	provider: {
		type: String,
		required: 'Provider is required',
        default: 'local'
	},
	
	roles: {
		type: [{
			type: String,
			enum: ['basic', 'admin'],
            default: 'basic'
		}],
		required: 'account role must be specified'
	},
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	},
	/* For reset password */
	resetPasswordToken: {
		type: String
	},
  	resetPasswordExpires: {
  		type: Date
  	}
});

/**
 * Hook a pre save method to hash the password
 */
AccountSchema.pre('save', function(next) {
	if (this.password && this.password.length > 6) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}

	next();
});

/**
 * Create instance method for hashing a password
 */
AccountSchema.methods.hashPassword = function(password) {
	if (this.salt && password) {
		return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
	} else {
		return password;
	}
};

/**
 * Create instance method for authenticating Account
 */
AccountSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

/**
 * Check Account name uniqueness
 */

AccountSchema.statics.ensureUniqueAccountName = function(account_name) {
    var _this = this;

    _this.findOne({
        account_name:account_name
    }, function(err, Account) {
        if (!err) {
            if(!Account) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    });
};


AccountSchema.statics.ensureUniqueEmail = function(email) {
    var _this = this;


    _this.findOne({
        email:email
    }, function(err, Account) {
        if (!err) {
            if(!Account) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    });
};
/**
 * Find possible not used Accountname
 */
AccountSchema.statics.findUniqueAccountname = function(accountname, suffix, callback) {
    var _this = this;
    var possibleAccountname = accountname + (suffix || '');

    _this.findOne({
        account_name: possibleAccountname
    }, function(err, Account) {
        if (!err) {
            if (!Account) {
                callback(possibleAccountname);
            } else {
                return _this.findUniqueAccountname(accountname, (suffix || 0) + 1, callback);
            }
        } else {
            callback(null);
        }
    });
};

AccountSchema.statics.findUniqueEmail = function(email, suffix, callback) {
    var _this = this;
    var possibleEmail = email + (suffix || '');

    _this.findOne({
        email: possibleEmail
    }, function(err, Account) {
        if (!err) {
            if (!Account) {
                callback(possibleEmail);
            } else {
                return _this.findUniqueEmail(email, (suffix || 0) + 1, callback);
            }
        } else {
            callback(null);
        }
    });
};

var Account = mongoose.model('Account', AccountSchema);

module.exports = Account;