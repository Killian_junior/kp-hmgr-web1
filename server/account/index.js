/**
 * Created by Dominic on 06-Oct-2014.
 */

var _ = require('lodash');

module.exports = _.extend(
    require('./services/account-manager')
);