/**
 * Created by Dominic on 06-Oct-2014.
 */
'use strict';

var Account = require('../models/account');

exports.createAccount = function (args, done) {

    var account = new Account(args);
    // Add missing user fields
    account.provider = 'local';
    // Then save the user
    account.save(function (err) {
        if (err) {
            return done(err, null);
        } else {
            // Remove sensitive data before login
            account.password = undefined;
            account.salt = undefined;

            return done(null, account);
        }
    });
};

exports.changePassword = function (args, done) {
    if (args) {
        Account.findById(args._id, function (err, account) {
            if (!err && account) {
                if (account.authenticate(args.currentPassword)) {
                    if (args.newPassword === args.verifyPassword) {
                        account.password = args.newPassword;
                        account.save(function (err) {
                            if (err) {
                                return done(err, null);
                            } else {
                                account.password = undefined;
                                account.salt = undefined;

                                return done(null, account);
                            }
                        });
                    } else {
                        return done({
                            message: 'Passwords do not match'
                        }, null);
                    }
                } else {
                    return done({
                        message: 'Current password is incorrect'
                    }, null);
                }
            } else {
                return done({
                    message: 'Account does not exist'
                }, null);
            }
        });

    }
};

exports.getAccountById = function (accountId, done) {

        Account.findById(accountId, function (err, account) {
            if (!err && account) {
                account.password = undefined;
                account.salt = undefined;
                return done(null, account);

            }
            if (err) {
                return done(err, null);
            }

            if (!account) {
                return done({
                    message: 'Account does not exist'
                }, null);
            }

        });


};


exports.getAccountByName = function (account_name, done) {
    if (account_name) {
        Account.findOne({
            account_name: account_name
        }, function (err, account) {
            if (!err && account) {
                account.password = undefined;
                account.salt = undefined;
                return done(null, account);

            }
            if (err) {
                return done(err, null);
            }

            if (!account) {
                return done({
                    message: 'Account does not exist'
                }, null);
            }

        });

    }
};


exports.authenticate = function (account_name, password, done) {
    Account.findOne({
        account_name: account_name
    }, function (err, account) {
        if (!err && account) {
            if (account.authenticate(password)) {
                account.password = undefined;
                account.salt = undefined;
                return done(null, account);
            } else {
                return done({
                    message: 'Passwords do not match'
                }, null);
            }

        }
        else {
            return done({
                message: 'Account does not exist'
            }, null);
        }

    });

};

exports.validateAccountName = function(account_name){
    return Account.ensureUniqueAccountName(account_name);
};

exports.validateAccountEmail = function(email){
    return Account.ensureUniqueEmail(email);
};

exports.initialize = function(){
    Account.find({}).exec(function(err, result){
        if(result.length === 0){
            Account.create({
                account_name: 'kp_admin',
                email: 'kp_admin@killianandpartners.com',
                password:'kp_admin',
                provider: 'local',
                roles: ['admin']
            }, function(err, result){
                if(err){
                    console.log(err);
                }

                if(result){
                    console.log('Account repo initialized successfully');
                }
            });
        }
    });
};