/**
 * Created by TERMINAL 7 on 10/14/2014.
 */

var hrEmployeeManager = require('hr-employee');
var CredentialObject = require('.dto/credential.dto');
var errorHandler = require('../../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var credential = new CredentialObject(req.body);
    credential.created_by = req.user.id;

    hrEmployeeManager.write.createCredential(credential, function(err, credential){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(credential);
        }
    });
};

exports.update = function(req, res){
    var data = new CredentialObject(req.body);

    hrEmployeeManager.write.updateCredential(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var credentialId = req.params.credentialId;

    hrEmployeeManager.write.removeCredentialById(credentialId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var credentialId = req.params.credentialId;
    hrEmployeeManager.read.getCredential(credentialId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var credentials = req.params.credentials;
    hrEmployeeManager.read.getCredentials(credentials, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};