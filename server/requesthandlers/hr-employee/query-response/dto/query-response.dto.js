/**
 * Created by TERMINAL 7 on 10/15/2014.
 */
var QueryResponseObject = function(args){

    var response = {};

    response.subject = args.subject;
    response.message = args.message;
    response.remarks = args.remarks;
    response.from = args.from;
    response.query_date = args.query_date;
    response.created = args.created;
    response.created_by = args.created_by;
    response.profile = args.profile;
    response.responses = args.responses;

    return response;
};

module.exports = QueryResponseObject;