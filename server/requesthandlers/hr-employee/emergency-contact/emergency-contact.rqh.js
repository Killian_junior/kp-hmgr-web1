/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
 var hrEmployeeManager = require('hr-employee');
 var EmergencyContactObject = require('.dto/emergency-contact.dto');
var errorHandler = require('../../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var emergencyContact = new EmergencyContactObject(req.body);
    emergencyContact.created_by = req.user.id;

    hrEmployeeManager.write.createEmergencyContact(emergencyContact, function(err, emergencyContact){
       if(err){
           return res.status(400).send({
               message: errorHandler.getErrorMessage(err)
           });
       } else {
           res.jsonp(emergencyContact);
       }
    });
};

exports.update = function(req, res){
    var data = new EmergencyContactObject(req.body);
    
    hrEmployeeManager.write.updateEmergencyContact(data, function(err, result){
       if(err){
           return res.status(400).send({
               message: errorHandler.getErrorMessage(err)
           });
       } else {
           res.jsonp(result);
       }
    });
};

exports.remove = function(req, res){
    var emergencyContactId = req.params.emergencyContactId;
    
    hrEmployeeManager.write.removeEmergencyContactById(emergencyContactId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var emergencyContactId = req.params.emergencyContactId;
    hrEmployeeManager.read.getEmergencyContact(emergencyContactId, function(err, result){
     if(err){
         return res.status(400).send({
             message: errorHandler.getErrorMessage(err)
         });
     }   else{
         res.jsonp(result);
     }
    });
};

exports.getAll = function(req, res){
    var emergencyContacts = req.params.emergencyContacts;
    hrEmployeeManager.read.getEmergencyContacts(emergencyContacts, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};