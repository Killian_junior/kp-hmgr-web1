/**
 * Created by TERMINAL 7 on 10/15/2014.
 */
var EmergencyContactObject = function(args){

    var emergency = {};

    emergency.house_number = args.house_number;
    emergency.street_name = args.street_name;
    emergency.city_locality = args.city_locality;
    emergency.residence_country = args.residence_country;
    emergency.remarks = args.remarks;
    emergency.created = args.created;
    emergency.created_by = args.created_by;
    emergency.personal = args.personal;

    return emergency;
};

module.exports = EmergencyContactObject;