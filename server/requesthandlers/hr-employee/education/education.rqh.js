/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
 var hrEmployeeManager = require('hr-employee');
 var EducationObject = require('.dto/education.dto');
var errorHandler = require('../../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var education = new EducationObject(req.body);
    education.created_by = req.user.id;

    hrEmployeeManager.write.createEducation(education, function(err, education){
       if(err){
           return res.status(400).send({
               message: errorHandler.getErrorMessage(err)
           });
       } else {
           res.jsonp(education);
       }
    });
};

exports.update = function(req, res){
    var data = new EducationObject(req.body);
    
    hrEmployeeManager.write.updateEducation(data, function(err, result){
       if(err){
           return res.status(400).send({
               message: errorHandler.getErrorMessage(err)
           });
       } else {
           res.jsonp(result);
       }
    });
};

exports.remove = function(req, res){
    var educationId = req.params.educationId;
    
    hrEmployeeManager.write.removeEducationById(educationId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var educationId = req.params.educationId;
    hrEmployeeManager.read.getEducation(educationId, function(err, result){
     if(err){
         return res.status(400).send({
             message: errorHandler.getErrorMessage(err)
         });
     }   else{
         res.jsonp(result);
     }
    });
};

exports.getAll = function(req, res){
    var educations = req.params.educations;
    hrEmployeeManager.read.getEducations(educations, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};