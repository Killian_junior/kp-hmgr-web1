/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
 var hrEmployeeManager = require('hr-employee');
 var PersonalObject = require('.dto/personal.dto');
var errorHandler = require('../../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var person = new PersonalObject(req.body);
    person.created_by = req.user.id;

    hrEmployeeManager.write.createPerson(person, function(err, person){
       if(err){
           return res.status(400).send({
               message: errorHandler.getErrorMessage(err)
           });
       } else {
           res.jsonp(person);
       }
    });
};

exports.update = function(req, res){
    var data = new PersonalObject(req.body);
    
    hrEmployeeManager.write.updatePerson(data, function(err, result){
       if(err){
           return res.status(400).send({
               message: errorHandler.getErrorMessage(err)
           });
       } else {
           res.jsonp(result);
       }
    });
};

exports.remove = function(req, res){
    var personId = req.params.personId;
    
    hrEmployeeManager.write.removePersonalById(personId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var personId = req.params.personId;
    hrEmployeeManager.read.getPersonal(personId, function(err, result){
     if(err){
         return res.status(400).send({
             message: errorHandler.getErrorMessage(err)
         });
     }   else{
         res.jsonp(result);
     }
    });
};

exports.getAll = function(req, res){
    var persons = req.params.persons;
    hrEmployeeManager.read.getPersonals(persons, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};