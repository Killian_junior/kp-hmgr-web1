/**
 * Created by TERMINAL 7 on 10/15/2014.
 */

var AssociationMembershipObject = function(args){

    var membership = {};

    membership.from = args.from;
    membership.to = args.to;
    membership.institution = args.institution;
    membership.course = args.course;
    membership.remarks = args.remarks;
    membership.membership_type = args.membership_type;
    membership.created = args.created;
    membership.created_by = args.created_by;
    membership.personal = args.personal;

    return membership;
};

module.exports = AssociationMembershipObject;