/**
 * Created by TERMINAL 7 on 10/15/2014.
 */
var CertificationObject = function(args){

    var certification = {};

    certification.certification_name = args.certification_name;
    certification.certification_type = args.certification_type;
    certification.awarding_institution = args.awarding_institution;
    certification.remarks = args.remarks;
    certification.created = args.created;
    certification.created_by = args.created_by;
    certification.profile = args.profile;

    return certification;
};

module.exports = CertificationObject;