/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
 var hrEmployeeManager = require('hr-employee');
 var CertificationObject = require('.dto/certification.dto');
var errorHandler = require('../../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var certification = new CertificationObject(req.body);
    certification.created_by = req.user.id;

    hrEmployeeManager.write.createCertification(certification, function(err, certification){
       if(err){
           return res.status(400).send({
               message: errorHandler.getErrorMessage(err)
           });
       } else {
           res.jsonp(certification);
       }
    });
};

exports.update = function(req, res){
    var data = new CertificationObject(req.body);
    
    hrEmployeeManager.write.updateCertification(data, function(err, result){
       if(err){
           return res.status(400).send({
               message: errorHandler.getErrorMessage(err)
           });
       } else {
           res.jsonp(result);
       }
    });
};

exports.remove = function(req, res){
    var certificationId = req.params.certificationId;
    
    hrEmployeeManager.write.removeCertificationById(certificationId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var certificationId = req.params.certificationId;
    hrEmployeeManager.read.getCertification(certificationId, function(err, result){
     if(err){
         return res.status(400).send({
             message: errorHandler.getErrorMessage(err)
         });
     }   else{
         res.jsonp(result);
     }
    });
};

exports.getAll = function(req, res){
    var certifications = req.params.certifications;
    hrEmployeeManager.read.getCertifications(certifications, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};