/**
 * Created by TERMINAL 7 on 10/14/2014.
 */

var hrEmployeeManager = require('hr-employee');
var TrainingObject = require('.dto/training.dto');
var errorHandler = require('../../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var training = new TrainingObject(req.body);
    training.created_by = req.user.id;

    hrEmployeeManager.write.createTraining(training, function(err, training){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(training);
        }
    });
};

exports.update = function(req, res){
    var data = new TrainingObject(req.body);

    hrEmployeeManager.write.updateTraining(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var trainingId = req.params.trainingId;

    hrEmployeeManager.write.removeTraining(trainingId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var trainingId = req.params.trainingId;
    hrEmployeeManager.read.getTraining(trainingId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var trainings = req.params.trainings;
    hrEmployeeManager.read.getTrainings(trainings, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};