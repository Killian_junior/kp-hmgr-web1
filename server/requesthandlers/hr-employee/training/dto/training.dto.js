/**
 * Created by TERMINAL 7 on 10/15/2014.
 */
var TrainingObject = function(args){

    var training = {};

    training.from = args.from;
    training.to = args.to;
    training.institution = args.institution;
    training.course = args.course;
    training.remarks = args.remarks;
    training.job_role = args.job_role;
    training.created = args.created;
    training.created_by = args.created_by;
    training.profile = args.profile;

    return training;
};

module.exports = TrainingObject;