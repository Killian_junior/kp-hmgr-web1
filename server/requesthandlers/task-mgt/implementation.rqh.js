/**
 * Created by Dominic on 05-Oct-2014.
 */



'use strict';

var taskServices = require('task-management'),
    errorHandler = require('../../config/errors'),
    ImplementationObject = require('./dtos/implementation.dto'),
    system = require('../system'),
    _ = require('lodash');

exports.get = function(req, res){
    var implementationId = req.params.implementationId;
    taskServices.read.getImplementations(implementationId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var taskId = req.params.taskId;

    taskServices.read.getImplementations(taskId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};


exports.create = function(req, res){
    var implementation = new ImplementationObject(req.body);
    implementation.created_by = req.user.id;
    taskServices.write.createImplementation(implementation, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.update = function(req, res){
    var implementation = new ImplementationObject(req.body);
    taskServices.write.updateImplementation(implementation, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};


exports.markSatisfied = function(req, res){
   var implementationId = req.params.implementationId;
    taskServices.write.markSatisfied(implementationId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var implementationId = req.params.implementationId;
    taskServices.write.removeImplementation(implementationId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.approve = function(req, res){
    var implementationId = req.params.implementationId;
    taskServices.write.approveImplementation(implementationId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.comment = function(req, res){
    var implementationId = req.params.implementationId;
    var comment = req.body;
    comment._id = implementationId;
    taskServices.write.commentImplementation(comment, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};


exports.hasAuthorization = function(req, res, next) {

    var implementationId = req.params.implementationId;
    taskServices.read.getImplementation(implementationId, function(err, result){
        if(result){
            if (req.user.id !== result.created_by) {
                return res.status(403).send({
                    message: 'User is not authorized'
                });
            }
        }

        next();
    });

};

exports.hasBossAuthorization = function(req, res, next) {
//
    var implementationId = req.params.implementationId;
    taskServices.read.getImplementation(implementationId, function(err, result){
        if(result){
            taskServices.read.getTask(result.task_id, function(err, task){
                if (req.user.id !== task.created_by) {
                    return res.status(403).send({
                        message: 'User is not authorized'
                    });
                }
            });

        }
        next();
    });

};