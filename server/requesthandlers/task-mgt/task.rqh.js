/**
 * Created by Dominic on 05-Oct-2014.
 */


'use strict';

var taskServices = require('task-management'),
    errorHandler = require('../../config/errors'),
    TaskObject = require('./dtos/task.dto'),
    _ = require('lodash');

exports.getAll = function(req, res){
    taskServices.read.getTasks(req.user.id, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var taskId = req.params.taskId;

    taskServices.read.getTask(taskId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};


exports.getCreatedTasks = function(req, res){
    var userId = req.user.id;
    taskServices.read.getCreatedTasks(userId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.create = function(req, res){
    var task = new TaskObject(req.body);
    task.created_by = req.user.id;
    taskServices.write.createTask(task, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.update = function(req, res){
    var task = new TaskObject(req.body);
    taskServices.write.updateTask(task, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};


exports.updateTaskStatus = function(req, res){
    var task = new TaskObject(req.body);
    taskServices.write.updateTaskStatus(task, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.updateTaskCompletion = function(req, res){
    var task = new TaskObject(req.body);
    taskServices.write.updateTaskCompletionDate(task, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var taskId = req.params.taskId;
    taskServices.write.removeTask(taskId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.hasAuthorization = function(req, res, next) {
    var taskId = req.params.taskId;
    taskServices.read.getTask(taskId, function(err, result){
        if(result){
            if (req.user.id !== result.created_by) {
                return res.status(403).send({
                    message: 'User is not authorized'
                });
            }
        }

        next();
    });

};
