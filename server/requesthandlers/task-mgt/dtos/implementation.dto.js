/**
 * Created by Dominic on 05-Oct-2014.
 */

'use strict';

var ImplementationObject = function(args){

    var implementation = {};

    implementation.details = args.details;
    implementation.work_date = args.work_date;
    implementation.url = args.url;  //must be checked for vulnerability
    implementation.created_by = args.created_by;
    implementation.task_id = args.task_id;


    return implementation;
};

module.exports = ImplementationObject;