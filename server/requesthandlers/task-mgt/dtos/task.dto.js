/**
 * Created by Dominic on 05-Oct-2014.
 */

'use strict';

var TaskObject = function(args){

    var task = {};

    task.title = args.title;
    task.description = args.description;
    task.status = args.status;
    task.created = args.created;
    task.created_by = args.created_by;
    task.assigned_to = args.assigned_to;
    task.expected_completion_date = args.expected_completion_date;
    task.completion_date = args.completion_date;
    task.recommendations = args.recommendations;


    return task;
};

module.exports = TaskObject;