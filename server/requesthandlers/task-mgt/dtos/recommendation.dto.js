/**
 * Created by Dominic on 05-Oct-2014.
 */

'use strict';

var RecommendationObject = function(args){

    var recommendation = {};

    recommendation.details = args.details;
    recommendation.work_date = args.work_date;
    recommendation._id = args._id;

    return recommendation;
};

module.exports = RecommendationObject;