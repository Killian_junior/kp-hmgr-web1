/**
 * Created by Dominic on 05-Oct-2014.
 */
'use strict';

var taskServices = require('task-management'),
    errorHandler = require('../../config/errors'),
    RecommendationObject = require('./dtos/recommendation.dto'),
    _ = require('lodash');


exports.create = function(req, res){
    var taskId =  req.params.taskId;
    var recommendation = new RecommendationObject(req.body);
    recommendation.created_by = req.user.id;
    taskServices.write.createRecommendation(taskId, recommendation, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.update = function(req, res){
    var taskId =  req.params.taskId;
    var recommendation = new RecommendationObject(req.body);
    taskServices.write.updateRecommendation(taskId, recommendation, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};


exports.remove = function(req, res){
    var taskId =  req.params.taskId;
    var recommendationId = req.body._id;
    taskServices.write.removeRecommendation(taskId, recommendationId, function(err, result){
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};
