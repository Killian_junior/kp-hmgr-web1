/**
 * Created by Dominic on 06-Oct-2014.
 */
'use strict';

var PasswordObject = function(args){

    var password = {};
    
    password.password = args.password;
    password.newPassword = args.newPassword;
    password.verifyPassword = args.verifyPassword;

    return password;

};

module.exports = PasswordObject;