/**
 * Created by Dominic on 06-Oct-2014.
 */
'use strict';

var AccountObject = function(args){

    var account = {};

    account.account_name = args.account_name;
    account.email = args.email;
    account.password = args.password;
    account.provider = 'local';
    account.roles = args.roles || ['basic'];
    return account;

};

module.exports = AccountObject;