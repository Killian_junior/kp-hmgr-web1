'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    AccountManager = require('../../account'),
    system = require('../system');

/**
 * User middleware
 */
exports.accountById = function(req, res, next, id) {
	AccountManager.getAccountById(id, function(err, account) {
		if (err) return next(err);
		if (!account) return next(new Error('Failed to load User ' + id));
		req.account = account;
		next();
	});
};

/**
 * Require login routing middleware
 */
//exports.requiresLogin = function(req, res, next) {
//	if (!req.isAuthenticated()) {
//		return res.status(401).send({
//			message: 'User is not logged in'
//		});
//	}
//
//	next();
//};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(roles) {
	var _this = this;

	return function(req, res, next) {
		system.requiresLogin(req, res, function() {
			if (_.intersection(req.user.roles, roles).length) {
				return next();
			} else {
				return res.status(403).send({
					message: 'User is not authorized'
				});
			}
		});
	};
};