/**
 * Created by Dominic on 06-Oct-2014.
 */
'use strict';

var passport = require('passport'),
    AccountManager = require('../../account'),
    errorHandler = require('../../config/errors'),
    AccountObject = require('./dto/account.dto');

exports.signup = function(req, res) {
    // For security measurement we remove the roles from the req.body object
    // delete req.body.roles
    // Init Variables
    var account = new AccountObject(req.body);

   AccountManager.createAccount(account, function(err, account) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            req.login(account, function(err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.jsonp(account);
                }
            });
        }
    });
};


exports.signin = function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err || !user) {
            res.status(400).send(info);
        } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            req.login(user, function(err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.jsonp(user);
                }
            });
        }
    })(req, res, next);
};

/**
 * Signout
 */
exports.signout = function(req, res) {
    req.logout();
    res.redirect('/');
};