/**
 * Created by Dominic on 06-Oct-2014.
 */

var _ = require('lodash');

module.exports = _.extend(
    require('./account/authentication.rqh'),
    require('./account/authorization.rqh'),
    require('./account/credential-management.rqh')
);