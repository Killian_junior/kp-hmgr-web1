/**
 * Created by Dominic on 02-Oct-2014.
 */
'use strict';
var AccountManager = require('../account');

exports.initialize = function(req, res){
    AccountManager.initialize();
    res.render('index');
};


exports.requiresLogin = function(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.status(401).send({
            message: 'User is not logged in'
        });
    }

    next();
};