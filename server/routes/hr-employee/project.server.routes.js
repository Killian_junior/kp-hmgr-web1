/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var project = require('../requesthandlers/hr-employee/project');

module.exports = function(app){

    app.route('/project/create').post(project.createProject);

    app.route('/project/update/:projectId').put(project.updateProject);

    app.route('/project/get/:projectId').get(project.getProject);

    app.route('/project/getAll').get(project.getProjects);

    app.route('/project/delete/:projectId').delete(project.removeProjectById);
};