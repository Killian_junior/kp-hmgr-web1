/**
 * Created by TERMINAL 7 on 10/15/2014.
 */
var associationMembership = require('../requesthandlers/hr-employee/association-membership');

module.exports = function(app){

    app.route('/association-membership/create').post(associationMembership.createAssociationMembership);

    app.route('/association-membership/update/:association-membershipId').put(associationMembership.updateAssociationMembership);

    app.route('/association-membership/get/:association-membershipId').get(associationMembership.getAssociationMembership);

    app.route('/association-membership/getAll').get(associationMembership.getAssociationMemberships);

    app.route('/association-membership/delete/:association-membershipId').delete(associationMembership.removeAssociationMembershipById);
};