/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var education = require('../requesthandlers/hr-employee/education');

module.exports = function(app){

    app.route('/education/create').post(education.createEducation);

    app.route('/education/update/:educationId').put(education.updateEducation);

    app.route('/education/get/:educationId').get(education.getEducation);

    app.route('/education/getAll').get(education.getEducations);

    app.route('/education/delete/:educationId').delete(education.removeEducationById);
};