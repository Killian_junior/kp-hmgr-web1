/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var certification = require('../requesthandlers/hr-employee/certification');

module.exports = function(app){

    app.route('/certification/create').post(certification.createCertification);

    app.route('/certification/update/:certificationId').put(certification.updateCertification);

    app.route('/certification/get/:certificationId').get(certification.getCertification);

    app.route('/certification/getAll').get(certification.getCertifications);

    app.route('/certification/delete/:certificationId').delete(certification.removeCertificationById);
};