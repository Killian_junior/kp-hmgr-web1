/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var queryResponse = require('../requesthandlers/hr-employee/queryResponse');

module.exports = function(app){

    app.route('/queryResponse/create').post(queryResponse.createQueryResponse);

    app.route('/queryResponse/update/:queryResponseId').put(queryResponse.updateQueryResponse);

    app.route('/queryResponse/get/:queryResponseId').get(queryResponse.getQueryResponse);

    app.route('/queryResponse/getAll').get(queryResponse.getQueryResponses);

    app.route('/queryResponse/delete/:queryResponseId').delete(queryResponse.removeQueryResponse);
};