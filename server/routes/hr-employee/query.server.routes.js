/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var query = require('../requesthandlers/hr-employee/query');

module.exports = function(app){

    app.route('/query/create').post(query.createQuery);

    app.route('/query/update/:queryId').put(query.updateQuery);

    app.route('/query/get/:queryId').get(query.getQuery);

    app.route('/query/getAll').get(query.getQueries);

    app.route('/query/delete/:queryId').delete(query.removeQuery);
};