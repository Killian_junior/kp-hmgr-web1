/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var emergencyContact = require('../requesthandlers/hr-employee/emergency-contact');

module.exports = function(app){

    app.route('/emergency-contact/create').post(emergencyContact.createEmergencyContact);

    app.route('/emergency-contact/update/:emergency-contactId').put(emergencyContact.updateEmergencyContact);

    app.route('/emergency-contact/get/:emergency-contactId').get(emergencyContact.getEmergencyContact);

    app.route('/emergency-contact/getAll').get(emergencyContact.getEmergencyContacts);

    app.route('/emergency-contact/delete/:emergency-contactId').delete(emergencyContact.removeEmergencyContactById);
};