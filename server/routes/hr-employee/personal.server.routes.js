/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var personal = require('../requesthandlers/hr-employee/personal');

module.exports = function(app){

    app.route('/personal/create').post(personal.createPerson);

    app.route('/personal/update/:personalId').put(personal.updatePerson);

    app.route('/personal/get/:personalId').get(personal.getPersonal);

    app.route('/personal/getAll').get(personal.getPersonals);

    app.route('/personal/delete/:personId').delete(personal.removePersonalById);
};