/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var residence = require('../requesthandlers/hr-employee/residence');

module.exports = function(app){

    app.route('/residence/create').post(residence.createResidence);

    app.route('/residence/update/:residenceId').put(residence.updateResidence);

    app.route('/residence/get/:residenceId').get(residence.getResidence);

    app.route('/residence/getAll').get(residence.getAllResidence);

    app.route('/residence/delete/:residenceId').delete(residence.removeResidence);
};