/**
 * Created by Dominic on 05-Oct-2014.
 */
'use strict';
var rqh = require('../../requesthandlers/task-mgt/recommendation.rqh'),
    system = require('../../requesthandlers/system');

module.exports = function(app){

    app.route('/recommendation/:taskId')
        .post(system.requiresLogin, rqh.create)
        .put(system.requiresLogin, rqh.update)
        .delete(system.requiresLogin,rqh.remove);


};