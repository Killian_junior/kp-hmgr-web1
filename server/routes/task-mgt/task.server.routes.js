/**
 * Created by Dominic on 05-Oct-2014.
 */
'use strict';
var rqh = require('../../requesthandlers/task-mgt/task.rqh'),
    system = require('../../requesthandlers/system');


module.exports = function(app){
    app.route('/tasks')
        .get(system.requiresLogin, rqh.getAll)
        .post(system.requiresLogin, rqh.create);

    app.route('/task/:taskId')
        .get(system.requiresLogin, rqh.hasAuthorization,rqh.get)
        .put(system.requiresLogin, rqh.hasAuthorization, rqh.update)
        .delete(system.requiresLogin, rqh.hasAuthorization, rqh.remove);


};