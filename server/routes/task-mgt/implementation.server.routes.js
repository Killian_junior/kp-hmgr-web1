/**
 * Created by Dominic on 05-Oct-2014.
 */
'use strict';
var rqh = require('../../requesthandlers/task-mgt/implementation.rqh'),
    system = require('../../requesthandlers/system');

module.exports = function(app){
    app.route('/implementations')
        .get(system.requiresLogin, rqh.hasAuthorization,rqh.getAll)
        .post(system.requiresLogin, rqh.hasAuthorization,rqh.create);

    app.route('/implementation/:implementationId')
        .get(system.requiresLogin, rqh.hasAuthorization,rqh.get)
        .put(system.requiresLogin, rqh.hasAuthorization,rqh.update)
        .delete(system.requiresLogin, rqh.hasAuthorization,rqh.remove);

    app.route('/commentimplementation/:implementationId')
        .put(system.requiresLogin, rqh.hasBossAuthorization,rqh.comment);

    app.route('/approveimplementation/:implementationId')
        .put(system.requiresLogin, rqh.hasBossAuthorization,rqh.approve);

    app.route('/markimplementation/:implementationId')
        .put(system.requiresLogin, rqh.hasBossAuthorization,rqh.markSatisfied);

};