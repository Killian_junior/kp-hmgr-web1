'use strict';

/**
 * Module dependencies.
 */
var account = require('../requesthandlers/accounts');

module.exports = function(app) {
    
	// Account Routes
	

	// Setting up the accounts profile api
	app.route('/account/current').get(account.current);
//	app.route('/accounts').put(accounts.update);

	// Setting up the accounts password api
	app.route('/account/password').post(account.changePassword);
//	app.route('/auth/forgot').post(account.forgot);
//	app.route('/auth/reset/:token').get(account.validateResetToken);
//	app.route('/auth/reset/:token').post(account.reset);

	// Setting up the accounts authentication api
	app.route('/auth/signup').post(account.signup);
	app.route('/auth/signin').post(account.signin);
	app.route('/auth/signout').get(account.signout);

	
	// Finish by binding the user middleware
	app.param('userId', account.accountById);
};
