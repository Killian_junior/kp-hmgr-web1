'use strict';

// Setting up route
app.config(['$stateProvider',
	function($stateProvider) {
		// samples state routing
		$stateProvider.
        state('sample',
        {
            url: '/sample',
            templateUrl: 'modules/samples/views/sample.index.html'
        }).
		state('createsample', {
			url: '/create/sample',
			templateUrl: 'modules/samples/views/create.sample.html'
		}).
        state('updatesample', {
            url: '/update/sample',
            templateUrl: 'modules/samples/views/update.sample.html'
        }).
        state('samples', {
            url: '/sample',
            templateUrl: 'modules/samples/views/samples.html'
        });

	}
]);