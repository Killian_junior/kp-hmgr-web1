'use strict';

// Users service used for communicating with the users REST endpoint
app.factory('Account', ['$resource',
	function($resource) {
		return $resource('accounts', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);