'use strict';

// Setting up route
app.config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('account', {
			url: '/account',
			templateUrl: 'modules/account/views/account.html'
		}).
		state('password', {
			url: '/change/password',
			templateUrl: 'modules/account/views/change-password.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/account/views/authentication/signup.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/account/views/authentication/signin.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/account/views/password/forgot-password.html'
		}).
		state('reset-invlaid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/account/views/password/reset-password-invalid.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/account/views/password/reset-password-success.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/account/views/password/reset-password.html'
		});
	}
]);